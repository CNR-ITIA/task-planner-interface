#ifndef robot_node_01571432
#define robot_node_01571432

#include <ros/ros.h>
#include <mongocpp_easy_driver/mongocpp_easy_driver.h>
#include <task_planner_interface/agent_status.h>
#include <task_planner_interface/generic_skill.h>
#include <task_planner_interface/pick_skill.h>
#include <task_planner_interface/place_skill.h>
#include <task_planner_interface/go_to_skill.h>
#include <task_planner_interface/pickplace_skill.h>

#include <moveit/move_group_interface/move_group_interface.h>
#include <subscription_notifier/subscription_notifier.h>
#include <task_planner_interface_msgs/MotionTaskExecutionRequest.h>
#include <task_planner_interface_msgs/MotionTaskExecutionFeedback.h>
#include <task_planner_interface_msgs/MotionTaskExecutionRequestArray.h>
#include <std_msgs/Header.h>
#include <std_msgs/Bool.h>
#include <std_msgs/Float64.h>
#include <bsoncxx/v_noabi/bsoncxx/document/view.hpp>

#define SUCCESS 1
#define FAILURE 0

void newCommandCallback(const task_planner_interface_msgs::MotionTaskExecutionRequestArrayConstPtr& msg){}

int main(int argc, char **argv)
{
  ros::init(argc, argv, "robot_node");
  ros::NodeHandle nh;
  ros::NodeHandle pnh("~");


  /* Get params */

  std::string topic_request_from_planner;
  if (!pnh.getParam("topic_request_from_planner",topic_request_from_planner))
  {
    ROS_ERROR("topic_request_from_planner not defined");
    return 0;
  }
  std::string topic_feedback_to_planner;
  if (!pnh.getParam("topic_feedback_to_planner",topic_feedback_to_planner))
  {
    ROS_ERROR("topic_feedback_to_planner not defined");
    return 0;
  }

  std::string group_name="";
  if (!pnh.getParam("group_name",group_name))
  {
    ROS_ERROR_STREAM(pnh.getNamespace() << ": group_name not defined");
    return 0;
  }

  std::string home_position="home";
  if (!pnh.getParam("home_position",home_position))
  {
    ROS_ERROR_STREAM(pnh.getNamespace() << ": home_position not defined");
  }

  std::string retry_position=home_position;
  if (!pnh.getParam("retry_position",retry_position))
  {
    ROS_ERROR_STREAM(pnh.getNamespace() << ": retry_position not defined");
  }

  bool go_home_after_execution = false;
  if (!nh.getParam("go_home_after_execution",go_home_after_execution))
  {
    ROS_ERROR("go_home_after_execution not defined. Default: false");
  }

  std::string mongo_database="default_db";
  if (!nh.getParam("mongo_database",mongo_database))
  {
    ROS_ERROR_STREAM(pnh.getNamespace() << ": mongo_database not defined. Default: " << mongo_database);
  }
  else
  {
    ROS_WARN_STREAM(pnh.getNamespace() << ": mongo_database: " << mongo_database);
  }

  std::string mongo_collection_tasks="default_coll";
  if (!nh.getParam("mongo_collection_tasks",mongo_collection_tasks))
  {
    ROS_ERROR_STREAM(pnh.getNamespace() << ": mongo_collection_tasks not defined. Default: " << mongo_collection_tasks);
  }
  else
  {
    ROS_WARN_STREAM(pnh.getNamespace() << ": mongo_collection_results: " << mongo_collection_tasks);
  }

  std::string mongo_collection_results="default_coll2";
  if (!nh.getParam("mongo_collection_results",mongo_collection_results))
  {
    ROS_ERROR_STREAM(pnh.getNamespace() << ": mongo_collection_results not defined. Default: " << mongo_collection_results);
  }
  else
  {
    ROS_WARN_STREAM(pnh.getNamespace() << ": mongo_collection_results: " << mongo_collection_results);
  }

  int recipe_index=0;
  if (!nh.getParam("starting_recipe_number",recipe_index))
  {
    ROS_WARN_STREAM(pnh.getNamespace() << ": starting_recipe_id not set. Default: 0");
  }

  /* Subscribers and Publishers*/

  ros::Publisher feedback_pub=nh.advertise<task_planner_interface_msgs::MotionTaskExecutionFeedback>(topic_feedback_to_planner,1);
//  ros::Publisher idletime_pub=nh.advertise<std_msgs::Float64>(pnh.getNamespace()+"/idle_time",1);
  ros::Publisher cycletime_pub=nh.advertise<std_msgs::Float64>(pnh.getNamespace()+"/cycle_time",1);
  ros_helper::SubscriptionNotifier<task_planner_interface_msgs::MotionTaskExecutionRequestArray> command_notif(nh,topic_request_from_planner,1);
  command_notif.setAdvancedCallback(&newCommandCallback);


  /* Define AgentStatus */

  taskPlannerInterface::AgentStatusPtr agent_status(new taskPlannerInterface::AgentStatus());


  /* Define MongoHandler */

  mongocpp_easy_driver::mongoEasyDriver mongo_handler(mongo_database);


  /* Define necessary skills from those available in manipulation */

  taskPlannerInterface::skills::PickSkillPtr pick_skill(new taskPlannerInterface::skills::PickSkill(agent_status));
  taskPlannerInterface::skills::PlaceSkillPtr place_skill(new taskPlannerInterface::skills::PlaceSkill(agent_status));
  taskPlannerInterface::skills::PickPlaceSkillPtr pick_place_skill(new taskPlannerInterface::skills::PickPlaceSkill(agent_status));
  taskPlannerInterface::skills::GoToSkillPtr goto_skill(new taskPlannerInterface::skills::GoToSkill(agent_status));

  std::map<std::string, taskPlannerInterface::skills::GenericSkillPtr> skills;
  skills["pick"] = pick_skill;
  skills["place"] = place_skill;
  skills["pickplace"] = pick_place_skill;
  skills["goto"] = goto_skill;

  /* Define a goTo skill for moving home */
  taskPlannerInterface::skills::GoToSkillPtr basic_goto_skill(new taskPlannerInterface::skills::GoToSkill(agent_status));
  basic_goto_skill->init(group_name);

//  ROS_INFO_STREAM(pnh.getNamespace() << ": checking action servers for all skills");
//  for (auto const& it : skills)
//  {
//      it.second->init(group_name,"");
//  }

//  pick_skill->init(group_name);
//  place_skill->init(group_name);
//  pick_place_skill->init(group_name);
//  goto_skill->init(group_name);


  /* Ready to loop */

  task_planner_interface_msgs::MotionTaskExecutionRequestArray current_request_msg;
  task_planner_interface_msgs::MotionTaskExecutionRequest current_command;
  task_planner_interface_msgs::MotionTaskExecutionFeedbackPtr feedback_msg(new task_planner_interface_msgs::MotionTaskExecutionFeedback());
  int current_result = FAILURE;
  bool is_dispatching = false;
  ros::Duration t_total;
  ros::Time t_start = ros::Time::now();
  ros::Rate r(100);

  while(ros::ok())
  {
    t_total=ros::Time::now()-t_start;
    ROS_INFO_THROTTLE(60,"Alive. Current cycle time = %f", t_total.toSec());

    if (!command_notif.waitForANewData(ros::Duration(15.0)))
    {
      if (is_dispatching)
      {
        ROS_INFO_STREAM( pnh.getNamespace() << ": 15 seconds without new commands.");
      }
    }
    else
    {
      ROS_INFO_STREAM( pnh.getNamespace() << ": new command received!");
      if (!is_dispatching)
      {
        is_dispatching=true; // dispatching begins now
        t_start = ros::Time::now();
      }

      //agent_status->resetObjectInHand();
      current_request_msg = command_notif.getData();
      current_command=current_request_msg.tasks.at(0);
      taskPlannerInterface::skills::GenericSkillPtr current_skill;

      std::string skill_type;
      bsoncxx::stdx::optional<bsoncxx::document::value> bson_doc;
      if (mongo_handler.findOneProjected(mongo_collection_tasks,"name",current_command.task_id,"type",skill_type))
      {
        mongo_handler.findOne(mongo_collection_tasks,"name",current_command.task_id,bson_doc);
        auto it = skills.find(skill_type);
        if(it!=skills.end())
        {
          ros::Time t_now = ros::Time::now();
          it->second->setPropertiesFromBSON(bson_doc);
          ROS_INFO("time set properties: %f", (ros::Time::now()-t_now).toSec());
          it->second->init(group_name);
          ROS_INFO("time init+set: %f", (ros::Time::now()-t_now).toSec());
          current_skill = it->second;
        }
        else
        {
          ROS_FATAL_STREAM("Skill type:" << skill_type << " not defined. Abort plan.");
          break;
        }
      }
//      else if (!current_command.task_id.compare("syncronization"))
//      {
//        bool synchronized=false;
//        while (!synchronized){
//          if (syncro_notif.waitForANewData(ros::Duration(90)))
//            synchronized=syncro_notif.getData().data;
//        }
//        current_result = SUCCESS;
//      }
      else if (!current_command.task_id.compare("wait_for_input"))
      {

        std::cout << "Press Enter to continue:\n";
        std::cin.ignore();

        current_result = SUCCESS;
      }
      else if (!current_command.task_id.compare("end"))
      {
        if (!home_position.empty())
        {
          ROS_INFO_STREAM("home: " << home_position);
          for (unsigned int i_try=0;i_try<3;i_try++)
          {
            if (basic_goto_skill->sendDirectLocation(home_position))
              break;
            else
            {
              ros::Duration(2.0).sleep();
              ROS_WARN("Cannot move to home. Try: %d", i_try+1);
            }
          }
        }

        current_result = SUCCESS;
        t_total=ros::Time::now()-t_start;
        std_msgs::Float64Ptr totaltime_msg(new std_msgs::Float64());
        totaltime_msg->data=t_total.toSec();
        cycletime_pub.publish(totaltime_msg);
        is_dispatching=false;
        recipe_index++;

        ROS_WARN_STREAM(pnh.getNamespace() << ": plan completed in "<< t_total.toSec() <<" seconds. Waiting for a new recipe.");
      }
      else
      {
        ROS_FATAL_STREAM("Command " << current_command.task_id << " not defined. Abort plan.");
        break;
      }

      if (mongo_handler.findOneProjected(mongo_collection_tasks,"name",current_command.task_id,"type",skill_type))
      {
        for (unsigned int i_try=0;i_try<2;i_try++)
        {
          current_skill->execute();
          current_result=current_skill->getOutcome();
          if (current_result==SUCCESS)
            break;
          else
          {
            ROS_WARN_STREAM(pnh.getNamespace() << ": skill execution failed. Retrying from another position...");
            if (!retry_position.empty())
            {
              basic_goto_skill->sendDirectLocation(retry_position);
            }
            ros::Duration(2.0).sleep();
          }
        }
        if (current_result==FAILURE)
        {
          ROS_ERROR_STREAM(pnh.getNamespace() << ". skill failed. Time to move on... ");
          // current_result = SUCCESS; // uncomment if you want to continue anyways
          ros::Duration(1.0).sleep();
        }
        if (go_home_after_execution && !home_position.empty())
        {
          basic_goto_skill->sendDirectLocation(home_position);
        }

        auto bson_results = current_skill->getResultsAsBSON();
        mongo_handler.appendToBSON("agent",group_name,bson_results);
        std::string recipe_name = "recipe_"+std::to_string(recipe_index);
        mongo_handler.appendToBSON("recipe",recipe_name,bson_results);
        std::vector<std::string> concurrent_tasks=current_request_msg.tasks.at(0).human_tasks;
        mongo_handler.appendArrayToBSON("concurrent_tasks",concurrent_tasks,bson_results);

        mongo_handler.insertOne(mongo_collection_results,bson_results);

//        current_skill->setConcurrentTasksFromMsg(current_request_msg);
//        std::string recipe_name = "recipe_"+std::to_string(recipe_index);
//        mongo_handler.saveSkillResults(current_skill,group_name, recipe_name);
      }
      feedback_msg->cmd_id = current_request_msg.cmd_id;
      feedback_msg->result = current_result;
      feedback_pub.publish(feedback_msg);
    }
    ros::spinOnce();
    r.sleep();
  }

  return 0;
}

#endif

