#include <task_planner_interface/go_to_skill.h>

namespace taskPlannerInterface
{

namespace skills {

#define SUCCESS 1
#define FAILURE 0

GoToSkill::GoToSkill(){}

bool GoToSkill::execute()
{
  manipulation_msgs::GoToGoal location;
  location.location_names.clear();
  location.location_names=m_goal_location;
  ROS_INFO_STREAM("Command " << m_id << ": move to locations:");
  for (auto it=m_goal_location.begin(); it!=m_goal_location.end(); ++it)
  {
    std::cout << "- " << it->data() << std::endl;
  }

  m_goto_ac->sendGoalAndWait(location);
  if (m_goto_ac->getResult()->result<0)
  {
    m_outcome = FAILURE;
    m_duration_real=0.0;
    m_duration_planned=0.0;
    m_planning_time=0.0;
    m_path_length=0.0;
    ROS_ERROR_STREAM("Command " << m_id << ": cannot move to desired location.");
  }
  else
  {
    m_outcome = SUCCESS;
    m_duration_real=m_goto_ac->getResult()->actual_duration.toSec();
    m_duration_planned=m_goto_ac->getResult()->expected_execution_duration.toSec();
    m_planning_time=m_goto_ac->getResult()->planning_duration.toSec();
    m_path_length=m_goto_ac->getResult()->path_length;
    ROS_INFO_STREAM("Command " << m_id << ": move ok");
  }
  return (bool)m_outcome;
}

void GoToSkill::init(const std::string& group_name)
{
  if (m_job_name.empty())
  {
    m_job_name="go_to";
  }
  m_outcome=FAILURE;
  m_goto_ac.reset(new actionlib::SimpleActionClient<manipulation_msgs::GoToAction>("/go_to_location_server/"+group_name+"/"+m_job_name));
  m_goto_ac->waitForServer();

  ROS_INFO("Initialized goto skill for movegroup %s", group_name.c_str());
}

bool GoToSkill::setPropertiesFromBSON(bsoncxx::stdx::optional<bsoncxx::document::value>& bson_doc){
  if(!GenericSkill::setPropertiesFromBSON(bson_doc))
    return false;

  m_goal_location.clear();

//  for (auto it = ele_goal.get_array().value.begin(); it != ele_goal.get_array().value.end(); ++it)
//  {
//    m_goal_location.push_back(it->data());
//  }

  bsoncxx::document::element ele_goal{bson_doc->view()["goal"]};
  bsoncxx::array::view ele_goal_view{ele_goal.get_array().value};
  for (bsoncxx::array::element subdocument : ele_goal_view){
      //std::cout<< subdocument["FunctionalArea"].get_utf8().value << std::endl;
      m_goal_location.push_back(subdocument.get_utf8().value.to_string());
  }


  //m_goal_location=ele_goal.get_utf8().value.to_string();

  try
  {
    bsoncxx::document::element ele_job{bson_doc->view()["job_name"]};
    m_job_name=ele_job.get_utf8().value.to_string();
  }
  catch (...)
  {
    m_job_name="go_to";
  }

  return true;
}


void GoToSkill::setGoal(std::vector<std::string>& arg){ m_goal_location=arg; }

bool GoToSkill::sendDirectLocation(const std::string& goal_location)
{
  bool outcome = false; 
  manipulation_msgs::GoToGoal location;
  location.location_names.clear();
  location.location_names.push_back(goal_location);
  ROS_INFO_STREAM("Direct send: move to location " << goal_location);
  m_goto_ac->sendGoalAndWait(location,ros::Duration(60.0));

  if (m_goto_ac->getResult()->result<0)
  {
    outcome = false;
    ROS_ERROR("Direct send: cannot move to desired location.");
  }
  else
  {
    outcome = true;
    ROS_INFO("Direct send: move ok");
  }
  return outcome;
}

} // end namespace

} //end namespace
